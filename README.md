# Reverse Proxy Example

This project shall showcase how modern web applications are deployed behind a reverse proxy and what quirks you may encounter in doing so.

## How to run?

Check out the repository and simply type `docker-compose up`.

## Services

### NGINX reverse proxy

The NGINX is accepting requests from your host on port `9999` and proxies them over to the `WeatherAPI` running inside the docker container.

### WeatherAPI

The weather API is a simply dummy asp.net Core web app. It is also reachable from your host on port `5000`.