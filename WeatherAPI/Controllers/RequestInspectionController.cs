using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WeatherAPI.Models;

namespace WeatherAPI.Controllers;

public class RequestInspectionController : Controller
{
    private readonly ILogger<RequestInspectionController> _logger;

    public RequestInspectionController(ILogger<RequestInspectionController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        var req = HttpContext.Request;

        var requestMethod = this.HttpContext.Request.Method;

        var content = $"Request Method: {requestMethod}\n";
        content += $"Rquest Scheme: " + req.Scheme + "\n";

        content += $"Request Host: " + req.Host + "\n";

        var generatedUrl = Url.ActionLink("Index", "Home");
        content += $"Generated URL: {generatedUrl}\n";

        var headerListing = String.Join("\n", req.Headers.Select(h => h.Key + ": " + h.Value));
        content += "\n" + headerListing;

        return Content(content, "text/plain");
    }
}
